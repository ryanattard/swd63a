﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace DataAccess
{
    public class ItemsRepository : ConnectionClass
    {
        public ItemsRepository() : base() { }


        public List<Item> GetItems()
        {
            //DataAdapter
            //DataReader

            //if (MyConnection.State == System.Data.ConnectionState.Closed)
            //    MyConnection.Open();

            string sql = "SELECT id, name, price, category_fk From items";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            List<Item> myResults = new List<Item>();
            using (NpgsqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    myResults.Add(new Item()
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1),
                        Price = reader.GetDecimal(2),
                        Category_Fk = reader["Category_fk"] == null ? -1 : Convert.ToInt32(reader["Category_fk"])
                    });
                }
            }

            return myResults;

        }
        //SELECT id, name, price, category_fk From items where category_fk = 2

        public List<Item> GetItemsByCategory(int categoryId)
        {

            string sql = "SELECT id, name, price, category_fk From items where category_fk = @categoryId";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@categoryId", categoryId);

            List<Item> myResults = new List<Item>();
            using (NpgsqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    myResults.Add(new Item()
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1),
                        Price = reader.GetDecimal(2),
                        Category_Fk = reader["Category_fk"] == null ? -1 : Convert.ToInt32(reader["Category_fk"])
                    });
                }
            }

            return myResults;

        }

        public string GetCategoryName(int id)
        {
            string sql = "SELECT title From categories where id = @id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);

            //when you need to retrieve 1 object, you can use execute scalar
            string name = cmd.ExecuteScalar().ToString();

            return name;
        }

        public void AddItem(Item i)
        {
            string sql = "Insert into Items (Name, Price, Category_Fk) Values (@name, @price, @category)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@name", i.Name);
            cmd.Parameters.AddWithValue("@price", i.Price);
            cmd.Parameters.AddWithValue("@category", i.Category_Fk);

            cmd.ExecuteNonQuery();
             
        }


        public void DeleteItem(int id)
        {
            string sql = "Delete from items where id = @id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);

            //the command has to participate in the transaction
            cmd.Transaction = MyTransaction;

            cmd.ExecuteNonQuery();

        }

        public void UpdateItem(Item i)
        {
            string sql = "update items set Name = @name, Price = @price, Category_Fk = @category where id = @id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@name", i.Name);
            cmd.Parameters.AddWithValue("@price", i.Price);
            cmd.Parameters.AddWithValue("@category", i.Category_Fk);
            cmd.Parameters.AddWithValue("@id", i.Id);

            cmd.ExecuteNonQuery();

        }


    }
}
