﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Newtonsoft.Json;
using StackExchange.Redis;
using System.Security.Cryptography;

namespace DataAccess
{
    public class RedisRepository
    {
         public string ConnectionString = "redis-14991.c1.us-east1-2.gce.cloud.redislabs.com:14991,password=Kf9m4fNaKcHxRMKAU1mFLKGuPgNJsorM";
      //  public string ConnectionString = "localhost";
        ConnectionMultiplexer connection;

        public RedisRepository()
        {
            connection = ConnectionMultiplexer.Connect(ConnectionString);
        }


        public string StoreItems(List<Item> items)
        {
            var db = connection.GetDatabase();

            string serializedItems = JsonConvert.SerializeObject(items);

            db.StringSet("items",serializedItems );

            return HashValue(serializedItems);
        }

        public List<Item> LoadItems()
        {
            var db = connection.GetDatabase();
            if (String.IsNullOrEmpty(db.StringGet("items"))) return null;

            List<Item> items = JsonConvert.DeserializeObject<List<Item>>(db.StringGet("items"));

            return items;
        }



        public string HashValue(string v)
        {
            MD5 alg = MD5.Create();
            byte [] digest = alg.ComputeHash(Encoding.UTF32.GetBytes(v));
            return Convert.ToBase64String(digest);

        }

    }
}
