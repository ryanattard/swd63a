﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Grpc.Core;

namespace DataAccess
{
    public class PubSubRepository
    {
        private Topic GetOrCreateTopic(string name)
        {
            PublisherServiceApiClient client = PublisherServiceApiClient.Create();
            try
            {
                return client.GetTopic(new TopicName("progforcloud63at", name));
            }
            catch (RpcException ex)
            {
                if(ex.Status.StatusCode == StatusCode.NotFound)
                return client.CreateTopic(new TopicName("progforcloud63at", name));
            }
            return null;
        }


        public void PublishMessage(string message, string topicName)
        {
            PublisherServiceApiClient client = PublisherServiceApiClient.Create();
            Topic MyTopic = GetOrCreateTopic(topicName);

            List<PubsubMessage> myMessages = new List<PubsubMessage>();
            myMessages.Add(new PubsubMessage()
            {MessageId= Guid.NewGuid().ToString(),
            PublishTime = Google.Protobuf.WellKnownTypes.Timestamp.FromDateTime(DateTime.UtcNow),
                Data = ByteString.CopyFromUtf8(message)
            });

            try
            {
                client.Publish(MyTopic.TopicName, myMessages);
            }
            catch (Exception ex)
            {

            }

        }



        private Subscription GetOrCreateSubscription(string name, string topicName)
        {
            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();
            try
            {
                return client.GetSubscription(new SubscriptionName("progforcloud63at", name));
            }
            catch (RpcException ex)
            {
                if (ex.Status.StatusCode == StatusCode.NotFound)
                { return client.CreateSubscription(new SubscriptionName("progforcloud63at", name),
                        GetOrCreateTopic(topicName).TopicName,
                        null,
                        60);
                 }
            }
            return null;
        }


        public string PullMessage(string topicName, string subscriptionName)
        {
            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();
       

            var response = client.Pull(GetOrCreateSubscription(subscriptionName, topicName).SubscriptionName
                , true, 1);

            if (response.ReceivedMessages.Count > 0)
            {
                var message = response.ReceivedMessages.ElementAt(0).Message;
                var messageData = message.Data.ToStringUtf8();

                client.Acknowledge(GetOrCreateSubscription(subscriptionName, topicName).SubscriptionName,
                    response.ReceivedMessages.Select(x => x.AckId)
                    );

                return messageData;
            }

            else return "";


   
        }
    }
}
