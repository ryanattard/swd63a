﻿using Google.Cloud.Datastore.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class DataStoreController : Controller
    {
        // GET: DataStore
        
        public ActionResult Index()
        {
            var dbStore = DatastoreDb.Create("progforcloud63at"); //getting an instance of the database

            Query q = new Query("User");

            var resultOfQuery = dbStore.RunQuery(q);
            List<UserLog> userlogs = new List<UserLog>();
            foreach(var user in resultOfQuery.Entities)
            {
                UserLog log = new UserLog()
                {
                    Id = user.Key.Path.First().Id,
                    Key = user.Key.ToString(),
                    Email = user["email"].StringValue,
                    LoggedOn = user["loggedOn"].TimestampValue.ToDateTime()
                   
                };

                try
                {
                    log.LoggedOut = user["loggedOut"].TimestampValue.ToDateTime();
                }
                catch
                {

                }


                userlogs.Add(log);
            }

            return View(userlogs);
        }

      
        public ActionResult Create()
        {

            var dbStore = DatastoreDb.Create("progforcloud63at"); //getting an instance of the database
            var objectManager = dbStore.CreateKeyFactory("User"); //creating or getting an instance of the User table


            var primaryKey = objectManager.CreateIncompleteKey(); //generating a new auto primary key
            Entity myNewUser = new Entity() //creating a new instance of User on-the-fly
            {
                Key = primaryKey,
                ["email"] = User.Identity.Name,
                ["loggedOn"] = DateTime.UtcNow
            };

            dbStore.Insert(myNewUser);

            return RedirectToAction("Index");
        }


        public void LogOut(string username)
        {
            var dbStore = DatastoreDb.Create("progforcloud63at"); //getting an instance of the database

            Query q = new Query("User");
            q.Filter = Google.Cloud.Datastore.V1.Filter.Property("email", username, PropertyFilter.Types.Operator.Equal);

            var resultOfQuery = dbStore.RunQuery(q);


            if(resultOfQuery != null)
            {
                var userLoggedIn = resultOfQuery.Entities.First();
                userLoggedIn["loggedOut"] = DateTime.UtcNow;

                dbStore.Update(userLoggedIn);

            }
        }


        public ActionResult Delete(long id )
        {
            var dbStore = DatastoreDb.Create("progforcloud63at"); //getting an instance of the database
            var objectManager = dbStore.CreateKeyFactory("User"); //creating or getting an instance of the User table


            Entity userToDelete = dbStore.Lookup(objectManager.CreateKey(id));

            dbStore.Delete(userToDelete);

            return RedirectToAction("Index");
        }
    }
}