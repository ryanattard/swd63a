﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class PubsubController : Controller
    {
        // GET: Pubsub
        public ActionResult Index()
        {
            PubSubRepository psp = new PubSubRepository();
           psp.PublishMessage("hello world", "TestTopic0C37DBB2956AB18F94C0");
            string message = psp.PullMessage("TestTopic0C37DBB2956AB18F94C0", "TestSubscription3l4kj12l4j");
            return View();
        }
    }
}