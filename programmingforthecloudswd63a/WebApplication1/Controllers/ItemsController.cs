﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccess;
using Common;
using Npgsql;

namespace WebApplication1.Controllers
{
    public class ItemsController : Controller
    {
        // GET: Items
        public ActionResult Index()
        {
            ItemsRepository ir = new ItemsRepository();
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

                var list = ir.GetItems();
                foreach(var item in list)
                {
                    var name = ir.GetCategoryName(item.Category_Fk);
                    item.Category = name;
                }


                return View(list);
            }
            catch(Exception ex)
            {
                ViewBag.Error = "Error occurred while querying items";
                return View(new List<Item>());
            }
            finally
            {
                if(ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }
           
        }

        public ActionResult IndexFromRedis()
        {
            RedisRepository rr = new RedisRepository();

            return View("Index", rr.LoadItems());
        }

        public ActionResult StoreInRedis()
        {
            ItemsRepository ir = new ItemsRepository();
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

                var list = ir.GetItems();
                foreach (var item in list)
                {
                    var name = ir.GetCategoryName(item.Category_Fk);
                    item.Category = name;
                }

                RedisRepository rr = new RedisRepository();
                rr.StoreItems(list);

                return View("Index",list);
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Error occurred while querying items";
                return View(new List<Item>());
            }
            finally
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }
        }




        public ActionResult ByCategory(int category)
        {
            ItemsRepository ir = new ItemsRepository();
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

                var list = ir.GetItemsByCategory(category);

                return View("Index", list);
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Error occurred while querying items";
                return View("Index", new List<Item>());
            }
            finally
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }

        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Item i)
        {
            ItemsRepository ir = new ItemsRepository();
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                    ir.MyConnection.Open();

                ir.AddItem(i);
                ViewBag.Success = "Item was created successfully";

                return View();
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Error occurred. Item was not added";
                return View();
            }
            finally
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }

        }

        public ActionResult DeleteAll(List<int> items)
        {
            ItemsRepository ir = new ItemsRepository();
            List<Item> myList = new List<Item>();
            try
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Closed)
                {
                    ir.MyConnection.Open();
                    ir.MyTransaction = ir.MyConnection.BeginTransaction();
                }

                foreach (int id in items)
                {
                    ir.DeleteItem(id);
                    
                }
                
                if(items.Count > 1)
                    ViewBag.Success = "Items were deleted successfully";
                else ViewBag.Success = "Item was deleted successfully";

                ir.MyTransaction.Commit();

                myList = ir.GetItems();

                return View("Index", myList);
            }
            catch (Exception ex)
            {
                ir.MyTransaction.Rollback();

                ViewBag.Error = "Error occurred. Item(s) were not deleted";

                myList = ir.GetItems();
                return View("Index", myList);
            }
            finally
            {
                if (ir.MyConnection.State == System.Data.ConnectionState.Open)
                    ir.MyConnection.Close();
            }
        }

        public ActionResult TestErrorReporting()
        {
            try
            {
                throw new Exception("Error thrown on purpose");
            }
            catch (Exception ex)
            {
                LoggingRepository.ReportError(ex);

            }

            return Content("done");

        }


    }
}