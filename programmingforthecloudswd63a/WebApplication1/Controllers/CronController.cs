﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace WebApplication1.Controllers
{
    public class CronController : Controller
    {
        // GET: Cron
        public ActionResult RefreshCache()
        {


            //1. Load Items From Cache

            RedisRepository rr = new RedisRepository();
            var items = rr.LoadItems();
         
            //2. Hash the serialized value of items 

            ItemsRepository ir = new ItemsRepository();
            ir.MyConnection.Open();
            var itemsFromDb = ir.GetItems();
            ir.MyConnection.Close();
               if(items == null)
            {
                rr.StoreItems(itemsFromDb);
                return Content("done");
            }

            //3. compare the digest produced with the digest produced earlier while stored in Application Variable
            if(rr.HashValue(JsonConvert.SerializeObject( items)) != 
                rr.HashValue(JsonConvert.SerializeObject(itemsFromDb))
                )
            {
            //4. if they do not match
                     // storeincache method and re-produce a new hashcode
                rr.StoreItems(itemsFromDb);  return Content("done");
            }

            return Content("items were not updated since they are still the same");
          

        }
    }
}